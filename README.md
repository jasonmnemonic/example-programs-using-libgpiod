# Example Programs using libGPIOd

Christophe BLAESS 2018


This project is a set of examples programs to access the GPIO lines using the new Linux API with the libGPIOd library.

The explaining (french) article will be released on October 22, 2018 at <https://www.blaess.fr/christophe/>.
